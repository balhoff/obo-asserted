# Demo "asserted" artifacts for various ontologies

These are not official releases of these ontologies. They are demonstrations of what a release of each ontology might look like which contains only the axioms asserted by that ontology project, and importing no external axioms.

* uberon.owl: unreasoned.owl with imports removed
* go.owl: enhanced.owl with imports removed (go-bridge, go-gci, x-disjoints merged in)
* cl.owl: cl-edit.owl with imports removed
* ro.owl: merge released ro modules; remove external import modules
* hp.owl: hp release with imports removed
* nbo.owl: nbo-edit.owl with imports removed, external content deleted